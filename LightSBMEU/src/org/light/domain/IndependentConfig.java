package org.light.domain;

import java.io.Serializable;

public abstract class IndependentConfig implements Comparable<IndependentConfig>,Cloneable,Serializable{
	private static final long serialVersionUID = 4498659246079600099L;
	protected String standardName;
	protected String fileName;
	protected String folder;
	@Override
	public int compareTo(IndependentConfig o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public abstract String generateImplString() throws Exception;
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
}
