package org.light.utils;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.Domain;
import org.light.exception.ValidateException;

public class DomainUtil {
	public static Domain findDomainInList(List<Domain> targets, String domainName) throws ValidateException{
		for (Domain d: targets){
			if (d.getStandardName().equals(domainName)) return d;
		}
		throw new ValidateException("域对象"+domainName+"不在列表中！");
	}
	
	public static Boolean inDomainList(Domain d,List<Domain> list){
		for (Domain dn :list){
			if (d.getStandardName().equals(dn.getStandardName())){
				return true;
			}
		}
		return false;
	}
	
	public static List<Domain> filterDataDomainList(List<List<Domain>> dataDomains, String domainName) throws ValidateException{
		List<Domain> results = new ArrayList<Domain>();
		for (List<Domain> list:dataDomains) {
			for (Domain d: list){
				if (d.getStandardName().equals(domainName)) results.add(d);
			}
		}
		return results;
	}
	
	public static  List<List<Domain>> removeDataDomainsFromLists(List<List<Domain>> dataDomains, String domainName) throws ValidateException{
		List<List<Domain>> results = new ArrayList<>();
		for (List<Domain> list:dataDomains) {	
			List<Domain> rows = new ArrayList<>();
			for (Domain d:list) {				
				if (!d.getStandardName().equals(domainName)) rows.add(d);
			}
			if (rows.size()>0)results.add(rows);
		}
		return results;
	}
	
	public static List<List<Domain>> addDataDomainToList(List<List<Domain>> dataDomains, Domain datadomain) throws ValidateException{
		List<List<Domain>> results = new ArrayList<>();
		boolean found = false;
		for (List<Domain> list:dataDomains) {
			if (list.size()>0) {
				if (list.get(0).getStandardName().equals(datadomain.getStandardName())) {
					list.add((Domain)datadomain.deepClone());
					found =true;
				}
				results.add(list);
			}
		}
		if (!found) {
			List<Domain> list = new ArrayList<>();
			list.add((Domain)datadomain.deepClone());
			results.add(list);
		}
		return results;
	}
	
	public static List<List<Domain>> deleteDataDomainFromList(List<List<Domain>> dataDomains, String datadomainname, int pos) throws ValidateException{
		List<List<Domain>> results = new ArrayList<>();
		for (List<Domain> list:dataDomains) {			
			if (list.get(0).getStandardName().equals(datadomainname)&&list.size()>pos) list.remove(pos);
			results.add(list);
		}
		return results;
	}
	
	public static List<List<Domain>> deleteAllDataDomainsFromList(List<List<Domain>> dataDomains, String datadomainname) throws ValidateException{
		List<List<Domain>> results = new ArrayList<>();
		for (List<Domain> list:dataDomains) {
			List<Domain> resultList = new ArrayList<>();
			for (Domain d: list) {
				if (!d.getStandardName().equals(datadomainname)) resultList.add(d);
			}
			if (resultList.size()>0) results.add(resultList);
		}
		return results;
	}
	
	public static List<List<Domain>> updateDataDomainFromList(List<List<Domain>> dataDomains, Domain datadomain,int pos) throws ValidateException{
		List<List<Domain>> results = new ArrayList<>();
		for (List<Domain> list:dataDomains) {			
			if (list.get(0).getStandardName().equals(datadomain.getStandardName())&&list.size()>pos) list.set(pos, (Domain)datadomain.deepClone());
			results.add(list);
		}
		return results;
	}
}
