package org.light.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.javaforever.poitranslator.core.ProjectExcelWorkBook;
import org.light.domain.Domain;
import org.light.domain.Project;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;
import org.light.wizard.ExcelWizard;
import org.light.wizard.ProjectWizard;

import com.hmkcode.vo.FileMeta;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.1
 *
 */
public class ExcelWizardFacade extends HttpServlet {
	private static final long serialVersionUID = -8679560776041236308L;

	private static Project instance = new Project();
	
	public static void setInstance(Project project) {
		instance = project;
	}
	
	public static Project getInstance() {
		return instance;
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExcelWizardFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String command = StringUtil.nullTrim(request.getParameter("command"));
		
		if ("createNewProject".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();
			params.put("fileName", StringUtil.nullTrim(request.getParameter("fileName")));
			params.put("project", StringUtil.nullTrim(request.getParameter("project")));
			params.put("packagetoken", StringUtil.nullTrim(request.getParameter("packagetoken")));
			params.put("dbprefix", StringUtil.nullTrim(request.getParameter("dbprefix")));
			params.put("dbname", StringUtil.nullTrim(request.getParameter("dbname")));
			params.put("dbusername", StringUtil.nullTrim(request.getParameter("dbusername")));
			params.put("dbpassword", StringUtil.nullTrim(request.getParameter("dbpassword")));
			params.put("dbtype", StringUtil.nullTrim(request.getParameter("dbtype")));
			params.put("technicalstack", StringUtil.nullTrim(request.getParameter("technicalstack")));
			params.put("title", StringUtil.nullTrim(request.getParameter("title")));
			params.put("subtitle", StringUtil.nullTrim(request.getParameter("subtitle")));
			params.put("footer", StringUtil.nullTrim(request.getParameter("footer")));
			params.put("crossorigin", StringUtil.nullTrim(request.getParameter("crossorigin")));
			params.put("resolution", StringUtil.nullTrim(request.getParameter("resolution")));
			params.put("domainsuffix", StringUtil.nullTrim(request.getParameter("domainsuffix")));
			params.put("daosuffix", StringUtil.nullTrim(request.getParameter("daosuffix")));
			params.put("daoimplsuffix", StringUtil.nullTrim(request.getParameter("daoimplsuffix")));
			params.put("servicesuffix", StringUtil.nullTrim(request.getParameter("servicesuffix")));
			params.put("serviceimplsuffix", StringUtil.nullTrim(request.getParameter("serviceimplsuffix")));
			params.put("controllersuffix", StringUtil.nullTrim(request.getParameter("controllersuffix")));
			params.put("domainnamingsuffix", StringUtil.nullTrim(request.getParameter("domainnamingsuffix")));
			params.put("controllernamingsuffix", StringUtil.nullTrim(request.getParameter("controllernamingsuffix")));
			params.put("language", StringUtil.nullTrim(request.getParameter("language")));
			params.put("schema", StringUtil.nullTrim(request.getParameter("schema")));

			ExcelWizardFacade.setInstance(ProjectWizard.createNewProject(params));	
			
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("mode", "createNewProject");
			out.print(JSONObject.fromObject(result));
		}
		
		if ("updateProject".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();
			params.put("fileName", StringUtil.nullTrim(request.getParameter("fileName")));
			params.put("project", StringUtil.nullTrim(request.getParameter("project")));
			params.put("packagetoken", StringUtil.nullTrim(request.getParameter("packagetoken")));
			params.put("dbprefix", StringUtil.nullTrim(request.getParameter("dbprefix")));
			params.put("dbname", StringUtil.nullTrim(request.getParameter("dbname")));
			params.put("dbusername", StringUtil.nullTrim(request.getParameter("dbusername")));
			params.put("dbpassword", StringUtil.nullTrim(request.getParameter("dbpassword")));
			params.put("dbtype", StringUtil.nullTrim(request.getParameter("dbtype")));
			params.put("technicalstack", StringUtil.nullTrim(request.getParameter("technicalstack")));
			params.put("title", StringUtil.nullTrim(request.getParameter("title")));
			params.put("subtitle", StringUtil.nullTrim(request.getParameter("subtitle")));
			params.put("footer", StringUtil.nullTrim(request.getParameter("footer")));
			params.put("crossorigin", StringUtil.nullTrim(request.getParameter("crossorigin")));
			params.put("resolution", StringUtil.nullTrim(request.getParameter("resolution")));
			params.put("domainsuffix", StringUtil.nullTrim(request.getParameter("domainsuffix")));
			params.put("daosuffix", StringUtil.nullTrim(request.getParameter("daosuffix")));
			params.put("daoimplsuffix", StringUtil.nullTrim(request.getParameter("daoimplsuffix")));
			params.put("servicesuffix", StringUtil.nullTrim(request.getParameter("servicesuffix")));
			params.put("serviceimplsuffix", StringUtil.nullTrim(request.getParameter("serviceimplsuffix")));
			params.put("controllersuffix", StringUtil.nullTrim(request.getParameter("controllersuffix")));
			params.put("domainnamingsuffix", StringUtil.nullTrim(request.getParameter("domainnamingsuffix")));
			params.put("controllernamingsuffix", StringUtil.nullTrim(request.getParameter("controllernamingsuffix")));
			params.put("language", StringUtil.nullTrim(request.getParameter("language")));
			params.put("schema", StringUtil.nullTrim(request.getParameter("schema")));

			ExcelWizardFacade.setInstance(ProjectWizard.updateProject(ExcelWizardFacade.getInstance(),params));	
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("mode", "updateProject");
			out.print(JSONObject.fromObject(result));
		}
		
		if ("createNewDomain".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("domain", StringUtil.nullTrim(request.getParameter("domain")));
			params.put("plural", StringUtil.nullTrim(request.getParameter("plural")));
			params.put("tableprefix", StringUtil.nullTrim(request.getParameter("tableprefix")));
			params.put("domainlabel", StringUtil.nullTrim(request.getParameter("domainlabel")));
			params.put("verbdenies", StringUtil.nullTrim(request.getParameter("verbdenies")));
			
			Domain domain = ProjectWizard.createNewDomain(params);
			try {
				ExcelWizardFacade.getInstance().addDomain(domain);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createNewDomain");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		

		if ("updateDomain".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("domain", StringUtil.nullTrim(request.getParameter("domain")));
			params.put("plural", StringUtil.nullTrim(request.getParameter("plural")));
			params.put("tableprefix", StringUtil.nullTrim(request.getParameter("tableprefix")));
			params.put("domainlabel", StringUtil.nullTrim(request.getParameter("domainlabel")));
			params.put("verbdenies", StringUtil.nullTrim(request.getParameter("verbdenies")));
			
			int serial = 0;
			Domain domain = null;
			try {
				if (!StringUtil.isBlank(request.getParameter("serial"))) serial = Integer.valueOf(request.getParameter("serial"));
				if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains().size()<=serial){
					ExcelWizardFacade.getInstance().addDomain(ProjectWizard.createNewDomain(params));
				}
				if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
					domain = ExcelWizardFacade.getInstance().getDomains().get(serial);
					domain = ProjectWizard.updateDomain(domain,params);
					ExcelWizardFacade.getInstance().updateDomain(serial, domain);
				}			
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateDomain");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("downloadExcel".equals(command)){
			String root = request.getSession().getServletContext().getRealPath("/").replace('\\', '/');
			String genExcelFile = ExcelWizardFacade.getInstance().getExcelTemplateName();
			if (StringUtil.isBlank(genExcelFile)) genExcelFile="gen.xls";
			try {
				System.out.println("outputdatadomains:"+ExcelWizardFacade.getInstance().getDataDomains().size());
				ExcelWizard.outputExcelWorkBook(ExcelWizardFacade.getInstance(), (root+ "source/").replace("\\", "/"),genExcelFile);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "downloadExcel");
				result.put("fileName",genExcelFile);
				out.print(JSONObject.fromObject(result));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
