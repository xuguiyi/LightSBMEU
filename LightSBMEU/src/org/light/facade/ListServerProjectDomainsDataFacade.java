package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Project;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.2
 *
 */
public class ListServerProjectDomainsDataFacade extends HttpServlet {
	private static final long serialVersionUID = -6496939187597601137L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListServerProjectDomainsDataFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			Project p = ExcelWizardFacade.getInstance();
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			
			result.put("fileName", p.getExcelTemplateName());
			result.put("project", p.getStandardName());
			result.put("packagetoken", p.getPackageToken());
			result.put("dbprefix", p.getDbPrefix());
			result.put("dbname", p.getDbName());
			result.put("dbusername", p.getDbUsername());
			result.put("dbpassword", p.getDbPassword());
			result.put("dbtype", p.getDbType());
			result.put("technicalstack", p.getTechnicalstack());
			result.put("title", p.getTitle());
			result.put("subtitle", p.getSubTitle());
			result.put("footer", p.getFooter());
			result.put("crossorigin", p.getCrossOrigin());
			result.put("resolution", p.getResolution());
			result.put("domainsuffix", p.getDomainSuffix());
			result.put("daosuffix", p.getDaoSuffix());
			result.put("daoimplsuffix", p.getDaoimplSuffix());
			result.put("servicesuffix", p.getServiceSuffix());
			result.put("serviceimplsuffix", p.getServiceimplSuffix());
			result.put("controllersuffix", p.getControllerSuffix());
			result.put("domainnamingsuffix", p.getDomainNamingSuffix());
			result.put("controllernamingsuffix", p.getControllerNamingSuffix());
			result.put("language", p.getLanguage());
			result.put("schema", p.getSchema());
			
			List<Domain> domains = p.getDomains();
			List<Map<String,String>> domainsData = new ArrayList<>();
			for (Domain d:domains) {
				Map<String,String> data = new TreeMap<>();
				data.put("domainName", d.getStandardName());
				data.put("plural", d.getPlural());
				data.put("tableprefix", d.getTablePrefix());
				data.put("domainlabel", d.getLabel());
				data.put("verbdenies", d.getVerbDeniesStr());
				domainsData.add(data);
			}
			result.put("domains",domainsData);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
