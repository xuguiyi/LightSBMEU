package org.light.limitedverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.exception.ValidateException;
import org.light.utils.InterVarUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class CountActiveRecords extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countActive" + domain.getCapFirstDomainName() + "Records");
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			Var countNum = new Var("countNum", new Type("int"));
			list.add(new Statement(100L, 1,
					"<select id=\"" + method.getLowerFirstMethodName() + "\" resultType=\"Integer\">"));
			list.add(new Statement(200L, 2,
					"select count(*) as " + countNum.getVarName() + " from " + domain.getDbPrefix()
							+ TableStringUtil.domainNametoTableName(domain) + " where "
							+ this.domain.getActive().getFeildNameAsTableColumn() + " = "
							+ this.domain.getDomainActiveStr() + ";"));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);

			return m.generateMethodString();
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countActive" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("Integer"));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.setThrowException(true);
			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countActive" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("Integer"));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countActive" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("Integer"));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.setThrowException(true);
			method.addMetaData("Override");

			Method daomethod = this.generateDaoMethodDefinition();
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(1000L, 2, "return "
					+ daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName()) + ";"));
			method.setMethodStatementList(WriteableUtil.merge(list));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public CountActiveRecords(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("ToggleOne");
		this.verbName = "countActive" + this.domain.getCapFirstDomainName() + "Records";
	}

	public CountActiveRecords() {
		super();
	}

}
