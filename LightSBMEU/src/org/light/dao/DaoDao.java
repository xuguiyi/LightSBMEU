package org.light.dao;

import java.util.List;

import org.light.core.Verb;
import org.light.domain.Domain;
import org.light.domain.Interface;

public interface DaoDao {
	public Interface generateDao(Domain domain, List<Verb> verbs) throws Exception;
	public Class generateDaoImpl(Domain domain, List<Verb> verbs) throws Exception;	
}
