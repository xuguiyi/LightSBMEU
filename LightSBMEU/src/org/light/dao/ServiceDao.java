package org.light.dao;

import java.util.List;

import org.light.domain.Domain;
import org.light.domain.Interface;
import org.light.domain.Method;
import org.light.domain.Naming;

public interface ServiceDao {
	public Interface generateService(Naming naming, Domain domain,List<Method> methods) throws Exception;
	public Class generateServiceImpl(Naming naming, Domain domain,List<Method> methods) throws Exception;
}
