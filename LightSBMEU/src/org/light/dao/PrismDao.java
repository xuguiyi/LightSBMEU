package org.light.dao;

import org.light.domain.Domain;
import org.light.domain.Naming;
import org.light.domain.Prism;

public interface PrismDao {
	public Prism generatePrism(Naming naming, String standardName,Domain domain) throws Exception;
	public void generatePrismFiles(Prism prism);
}
