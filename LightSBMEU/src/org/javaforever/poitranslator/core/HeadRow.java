package org.javaforever.poitranslator.core;

public class HeadRow {
	protected static String [] headerArr = {"用例编号","用例说明","动作","标签","值","是否跳过","测试结果"};

	public static String[] getHeaderArr() {
		return headerArr;
	}

	public static void setHeaderArr(String[] headerArr0) {
		headerArr = headerArr0;
	}
}
